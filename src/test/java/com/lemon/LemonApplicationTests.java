package com.lemon;

import com.lemon.web.rest.LogResource;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.stream.IntStream;

import static org.assertj.core.api.Fail.fail;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@EnableAutoConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LemonApplicationTests {

    @Autowired
    private LogResource logResource;

    @Autowired
    private MockMvc mvc;


    @Test
    @Order(1)
    void Case1() throws Exception {
        String name = "pepe";
        String userID = "case1";
        int limit = 5;
        successResponse(name, userID, limit, 4);
    }

    @Test
    @Order(2)
    void Case2() throws Exception {
        String name = "pepe";
        String userID = "case2";
        int limit = 5;
        IntStream.rangeClosed(1, 5)
                .boxed()
                .sorted(Collections.reverseOrder())
                .forEach(counter -> {
                    successResponse(name, userID, limit, counter - 1);
                });

    }

    @Test
    @Order(3)
    void Case3() throws Exception {
        String name = "pepe";
        String userID = "case3";
        int limit = 5;
        IntStream.rangeClosed(1, 5)
                .boxed()
                .sorted(Collections.reverseOrder())
                .forEach(counter -> {
                    successResponse(name, userID, limit, counter - 1);
                });
        toManyResponse(name, userID);

    }

    @Test
    @Order(4)
    void Case4() throws Exception {
        String name = "pepe";
        String userID = "case4";
        int limit = 5;
        IntStream.rangeClosed(1, 5)
                .boxed()
                .sorted(Collections.reverseOrder())
                .forEach(counter -> {
                    successResponse(name, userID, limit, counter - 1);
                });
        toManyResponse(name, userID);
        Thread.sleep(10000);
        successResponse(name, userID, limit, 4);

    }

    private void successResponse(String name, String userId, int limit, int remain) {
        try {
            mvc.perform(get("/api/v1/logs/" + name).contentType(MediaType.APPLICATION_JSON)
                    .header("userID", userId)
            ).andExpect(status().isOk()
            ).andExpect(header().longValue("X-RateLimit-Remaining", remain)
            ).andExpect(content().string(containsString("Check your fucking logs!"))
            ).andExpect(content().string(containsString(name))
            ).andExpect(header().longValue("X-RateLimit-Limit", limit));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    private void toManyResponse(String name, String userId) {
        try {
            mvc.perform(get("/api/v1/logs/" + name).contentType(MediaType.APPLICATION_JSON)
                    .header("userID", userId)
            ).andExpect(status().is(HttpStatus.TOO_MANY_REQUESTS.value()));

        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

}
