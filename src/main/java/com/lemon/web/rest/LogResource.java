package com.lemon.web.rest;

import com.lemon.domain.LogMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@RestController
@Slf4j
public class LogResource {
    /**
     * GET /api/v1/logs : get FOASS messages
     *
     * @param from String (Required)
     * @return LogMessage (status code 200)
     * or Bad Request (status code 400)
     * or Too Many Requests (status code 429)
     * or Internal Server Error (status code 500)
     */
    @GetMapping(value = "api/v1/logs/{from}")
    public ResponseEntity<String> getMessage(@PathVariable String from) {
        return ResponseEntity.ok("Check your fucking logs! " + from);

    }
}
