package com.lemon.config;

import com.lemon.domain.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


@Slf4j
public class RateLimitInterceptor extends HandlerInterceptorAdapter {


    private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(5);

    private Map<String, RateLimiter> limiters = new ConcurrentHashMap<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String userID = request.getHeader("userID");
        if (userID == null) {
            log.info("request without userID");
            response.sendError(HttpStatus.BAD_REQUEST.value(), " donde esta el user amigo?");
            return false;
        }
        log.info("request with userID={}", userID);
        RateLimiter rateLimiter = getRateLimiter(userID);
        boolean allowRequest = rateLimiter.tryAcquire();

        if (!allowRequest) {
            response.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
        }
        response.addHeader("X-RateLimit-Limit", String.valueOf(rateLimiter.getMaxPermits()));
        response.addHeader("X-RateLimit-Remaining", String.valueOf(rateLimiter.getAvailable()));
        return allowRequest;
    }

    private RateLimiter getRateLimiter(String userId) {
        RateLimiter rateLimiter = limiters.computeIfAbsent(userId, this::createRateLimiter);
        return rateLimiter;
    }

    private RateLimiter createRateLimiter(String userId) {
        log.info("Creating rate limiter for userID={}", userId);
        return RateLimiter.create(TimeUnit.SECONDS, scheduler, userId);
    }

}

