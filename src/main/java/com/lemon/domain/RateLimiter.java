package com.lemon.domain;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

@Data
@Slf4j
public class RateLimiter {

    @Value("${rate.time}")
    private int time = 10;

    @Value("${rate.max_permits=5}")
    private int maxPermits = 5;

    private Semaphore semaphore;
    private TimeUnit timePeriod;
    private ScheduledExecutorService scheduler;
    private String userId;


    private RateLimiter(TimeUnit timePeriod, ScheduledExecutorService scheduler, String userId) {
        this.semaphore = new Semaphore(this.maxPermits);
        this.timePeriod = timePeriod;
        this.scheduler = scheduler;
        this.userId = userId;
    }

    public static RateLimiter create(TimeUnit timePeriod, ScheduledExecutorService scheduler, String userId) {
        RateLimiter limiter = new RateLimiter(timePeriod, scheduler, userId);
        limiter.schedulePermitReplenishment();
        return limiter;
    }

    public boolean tryAcquire() {
        log.info("request for {} - maxPermits= {} - available={}", this.userId, this.maxPermits, this.semaphore.availablePermits());
        return semaphore.tryAcquire();
    }

    public int getAvailable() {
        return this.semaphore.availablePermits();
    }

    public void schedulePermitReplenishment() {

        this.scheduler.scheduleAtFixedRate(() -> {
            log.info("thead for {} - maxPermits= {} - available={}", this.userId, this.maxPermits, this.semaphore.availablePermits());
            semaphore.release(maxPermits - semaphore.availablePermits());
        }, time, time, timePeriod);

    }

}
